package cupq.utils;

import java.nio.file.Path;
import java.nio.file.Paths;

public class PathUtil {

    public static Path ROOT_PATH = Paths.get("/", "home", "jessewang");
    public static Path DATA_PATH = Paths.get(ROOT_PATH.toString(), "data", "cupq");
    public static Path NEW_TOKENS_PATH = Paths.get(DATA_PATH.toString(), "new_tokens.json");
    public static Path MEDICAL_JOURNALS_PATH = Paths.get(DATA_PATH.toString(), "medical_journals.json");
    public static Path SEARCH_SERVER_UPDATE_PATH = Paths.get(DATA_PATH.toString(), "search_server_update.json");

}
