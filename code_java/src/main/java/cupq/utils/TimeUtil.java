package cupq.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeUtil {

    public static void println(String message) {
        System.out.println(getTime() + " : " + message);
    }

    public static String getTime() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date());
    }

}
