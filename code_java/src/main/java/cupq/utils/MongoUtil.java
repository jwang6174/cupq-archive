package cupq.utils;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.gridfs.GridFSBucket;
import com.mongodb.client.gridfs.GridFSBuckets;
import org.bson.Document;

public class MongoUtil {

    public static final MongoClient CLIENT =
            MongoClients.create("mongodb://" + NetworkUtil.MONGO_HOST + ":" + NetworkUtil.MONGO_PORT);

    public static final MongoDatabase CUPQ_DB = CLIENT.getDatabase("cupq");

    public static final GridFSBucket CUPQ_FS = GridFSBuckets.create(CUPQ_DB);

    public static final MongoCollection<Document> ARTICLES_COLLECTION = CUPQ_DB.getCollection("articles");

    public static final MongoCollection<Document> SEARCH_CACHE_COLLECTION = CUPQ_DB.getCollection("search_cache");

}
