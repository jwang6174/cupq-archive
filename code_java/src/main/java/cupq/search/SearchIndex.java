package cupq.search;

import cupq.utils.MongoUtil;
import cupq.utils.PathUtil;
import org.bson.BsonString;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class SearchIndex {

    private final static Map<String, List<Posting>> SEARCH_INDEX = initSearchIndex();

    public static List<SearchResult> getSearchResults(List<String> tokens) {
        var searchResults = new HashMap<Long, SearchResult>();
        for (var token : tokens) {
            var postings = SEARCH_INDEX.get(token);
            if (postings != null) {
                for (var posting : postings) {
                    var articleMeta = ArticleMetas.getArticleMeta(posting.getPMID());
                    if (articleMeta != null) {
                        var searchResult = searchResults.getOrDefault(
                                posting.getPMID(),
                                new SearchResult(posting.getPMID(), articleMeta));
                        searchResult.add(posting);
                        searchResults.putIfAbsent(posting.getPMID(), searchResult);
                    }
                }
            } else {
                return Collections.unmodifiableList(new ArrayList<>());
            }
        }
        return Collections.unmodifiableList(List.copyOf(searchResults.values()));
    }

    private static Map<String, List<Posting>> initSearchIndex() {
        var searchIndex = new HashMap<String, List<Posting>>();
        MongoUtil.CUPQ_FS.find().forEach(gridFSFile -> {
            var token = gridFSFile.getId().asString().getValue();
            var postings = getPostings(token);
            if (postings != null) {
                searchIndex.put(token, postings);
            }
        });
        return searchIndex;
    }

    private static List<Posting> getPostings(String token) {
        try {
            var downloadStream = MongoUtil.CUPQ_FS.openDownloadStream(new BsonString(token));
            var bytesToWriteTo = downloadStream.readAllBytes();
            downloadStream.close();
            var postingObjects = (JSONArray) new JSONParser().parse(new String(bytesToWriteTo, StandardCharsets.UTF_8));
            var postings = new ArrayList<Posting>(postingObjects.size());
            for (var obj : postingObjects) {
                var posting = new Posting((JSONObject) obj);
                var articleMeta = ArticleMetas.getArticleMeta(posting.getPMID());
                if (articleMeta != null) {
                    postings.add(posting);
                }
            }
            return Collections.unmodifiableList(postings);
        } catch (ParseException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void updateSearchIndex() throws IOException, ParseException {
        var objects = (JSONArray) new JSONParser().parse(new FileReader(PathUtil.NEW_TOKENS_PATH.toString()));
        for (Object obj : objects) {
            var token = (String) obj;
            var postings = getPostings(token);
            if (postings != null) {
                SEARCH_INDEX.put(token, postings);
            }
        }
    }
}
