package cupq.search;

public class SearchResult {

    private final long PMID;
    private final ArticleMeta ARTICLE_META;
    private int tokenCount = 0;
    private int tokenCountTitle = 0;
    private double tokenScore = 0;
    private double overallScore = 0;

    public SearchResult(long pmid, ArticleMeta articleMeta) {
        PMID = pmid;
        ARTICLE_META = articleMeta;
    }

    public long getPMID() {
        return PMID;
    }

    public void add(Posting posting) {
        tokenCount++;
        if (posting.inTitle()) {
            tokenCountTitle++;
        }
        tokenScore = ((tokenScore * (tokenCount - 1)) + posting.getScore()) / tokenCount;
    }

    public int getTokenCount() {
        return tokenCount;
    }

    public int getTokenCountTitle() {
        return tokenCountTitle;
    }

    public double getTokenScore() {
        return tokenScore;
    }

    public ArticleMeta getArticleMeta() {
        return ARTICLE_META;
    }

    public void setOverallScore(double overallScore) {
        this.overallScore = overallScore;
    }

    public double getOverallScore() {
        return overallScore;
    }

}
