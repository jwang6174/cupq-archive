package cupq.search;

import java.util.*;

public class SearchRanking {

    public static List<SearchResult> processSearchResults(
            List<SearchResult> rawSearchResults,
            int tokenCount,
            int limit,
            double weightJournalImpactFactor,
            double weightJournalTotalCites,
            double weightDays,
            double weightTokenScore,
            double weightTokenCountTitle,
            double weightClinicalKeyword) {

        var processedSearchResults = new ArrayList<SearchResult>();

        // Only include search results that contain all tokens.
        for (var searchResult : rawSearchResults) {
            if (searchResult.getTokenCount() == tokenCount) {
                processedSearchResults.add(searchResult);
            }
        }

        // Store values for each ranking criteria.
        var normJournalImpactFactors = new ZScore();
        var normJournalTotalCites = new ZScore();
        var normDays = new ZScore();
        var normTokenScores = new ZScore();
        var normTokenCountTitle = new ZScore();

        // Collect scores for each ranking condition. Will be used for
        // score normalization later.
        for (var searchResult : processedSearchResults) {
            normJournalImpactFactors.add(searchResult.getArticleMeta().getJournal().getImpactFactor());
            normJournalTotalCites.add(searchResult.getArticleMeta().getJournal().getTotalCites());
            normDays.add(searchResult.getArticleMeta().getDays());
            normTokenScores.add(searchResult.getTokenScore());
            normTokenCountTitle.add(searchResult.getTokenCountTitle());
        }

        normJournalImpactFactors.markComplete();
        normJournalTotalCites.markComplete();
        normDays.markComplete();
        normTokenScores.markComplete();
        normTokenCountTitle.markComplete();

        // Calculate scores with Z-score normalization.
        for (var searchResult : processedSearchResults) {

            var overallScore = weightJournalImpactFactor * normJournalImpactFactors.normalize(
                    searchResult.getArticleMeta().getJournal().getImpactFactor());

            overallScore += weightJournalTotalCites * normJournalTotalCites.normalize(
                    searchResult.getArticleMeta().getJournal().getTotalCites());

            overallScore += weightDays * normDays.normalize(
                    searchResult.getArticleMeta().getDays());

            overallScore += weightTokenScore * normTokenScores.normalize(
                    searchResult.getTokenScore());

            overallScore += weightTokenCountTitle * normTokenCountTitle.normalize(
                    searchResult.getTokenCountTitle());

            if (searchResult.getArticleMeta().hasClinicalKeyword()) {
                overallScore += weightClinicalKeyword;
            }

            searchResult.setOverallScore(overallScore);
        }

        processedSearchResults.sort(Comparator.comparingDouble(SearchResult::getOverallScore).reversed());

        if (processedSearchResults.isEmpty()) {
            return Collections.unmodifiableList(processedSearchResults);
        } else {
            return Collections.unmodifiableList(
                    processedSearchResults.subList(0, Math.min(limit, processedSearchResults.size())));
        }
    }

}
