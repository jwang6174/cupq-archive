package cupq.search;

import cupq.utils.MongoUtil;
import cupq.utils.PathUtil;
import cupq.utils.TimeUtil;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.set;

public class SearchServerUpdateThread extends Thread {

    @Override
    public void run() {
        while (true) {
            try {
                var data = (JSONObject) new JSONParser().parse(
                        new FileReader(PathUtil.SEARCH_SERVER_UPDATE_PATH.toString()));

                if ((boolean) data.get("updates_available")) {

                    TimeUtil.println("INFO : starting search server update");

                    TimeUtil.println("INFO : updating article metas");
                    ArticleMetas.updateArticleMetas();

                    TimeUtil.println("INFO : updating search index");
                    SearchIndex.updateSearchIndex();

                    TimeUtil.println("INFO : updating search cache");
                    SearchCache.updateSearchCache();

                    TimeUtil.println("INFO : resetting pending articles");
                    resetPendingArticles();

                    TimeUtil.println("INFO : finished search server update");

                    Updates.notifyUpdatesComplete();
                }
            } catch (ParseException | IOException e) {
                e.printStackTrace();
            }
            try {
                Thread.sleep(60_000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void resetPendingArticles() {
        MongoUtil.ARTICLES_COLLECTION.updateMany(eq("pending", true), set("pending", false));
    }

}
