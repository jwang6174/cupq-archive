package cupq.search;

import org.bson.Document;

import java.util.*;

public class ArticleMeta {

    private static final Map<String, Integer> MONTHS = Collections.unmodifiableMap(Map.ofEntries(
            Map.entry("Jan", 1),
            Map.entry("Feb", 2),
            Map.entry("Mar", 3),
            Map.entry("Apr", 4),
            Map.entry("May", 5),
            Map.entry("Jun", 6),
            Map.entry("Jul", 7),
            Map.entry("Aug", 8),
            Map.entry("Sep", 9),
            Map.entry("Oct", 10),
            Map.entry("Nov", 11),
            Map.entry("Dec", 12)));

    private final long PMID;
    private final int DAYS;
    private final boolean IS_REVIEW;
    private final boolean IS_GUIDELINE;
    private final boolean IS_RCT;
    private final boolean HAS_CLINICAL_KEYWORD;
    private final Journal JOURNAL;

    public ArticleMeta(Document doc, Journal journal) {

        // Initialize PMID.
        PMID = doc.getInteger("_id").longValue();

        // Initialize DAYS.
        var pubYear = Integer.parseInt(doc.getString("pub_year"));
        var pubMonth = initPubMonthOrDay(doc.getString("pub_month"));
        var pubDay = initPubMonthOrDay(doc.getString("pub_day"));
        DAYS = (365 * pubYear) + (30 * (pubMonth - 1)) + pubDay;

        // Initialize IS_REVIEW and IS_GUIDELINE.
        var pubTypes = doc.getList("pub_types", String.class);
        var isReview = false;
        var isGuideline = false;
        var isRCT = false;
        for (var pubType : pubTypes) {
            if (pubType.equalsIgnoreCase("Review") ||
                    pubType.equalsIgnoreCase("Systematic Review") ||
                    pubType.equalsIgnoreCase("Meta-Analysis")) {
                isReview = true;
            }
            if (pubType.equalsIgnoreCase("Guideline") ||
                    pubType.equalsIgnoreCase("Practice Guideline")) {
                isGuideline = true;
            }
            if (pubType.equalsIgnoreCase("Randomized Controlled Trial")) {
                isRCT = true;
            }
        }
        IS_REVIEW = isReview;
        IS_GUIDELINE = isGuideline;
        IS_RCT = isRCT;

        // Initialize HAS_CLINICAL_KEYWORD.
        HAS_CLINICAL_KEYWORD = doc.getBoolean("has_clinical_keyword");

        // Initialize JOURNAL.
        JOURNAL = journal;
    }

    private int initPubMonthOrDay(String value) {
        if (!value.isEmpty()) {
            if (MONTHS.containsKey(value)) {
                return MONTHS.get(value);
            } else {
                return Integer.parseInt(value);
            }
        }
        return 1;
    }

    public long getPMID() {
        return PMID;
    }

    public int getDays() {
        return DAYS;
    }

    public boolean hasPubType(String pubType) {
        if (pubType.equalsIgnoreCase("Reviews") ||
                pubType.equalsIgnoreCase("Review")) {

            return IS_REVIEW;

        } else if (pubType.equalsIgnoreCase("Guidelines") ||
                pubType.equalsIgnoreCase("Guideline")) {

            return IS_GUIDELINE;

        } else if (pubType.equalsIgnoreCase("RCTs") ||
                pubType.equalsIgnoreCase("RCT")) {

            return IS_RCT;

        } else {
            return pubType.equalsIgnoreCase("All Types");
        }
    }

    public boolean hasClinicalKeyword() {
        return HAS_CLINICAL_KEYWORD;
    }

    public Journal getJournal() {
        return JOURNAL;
    }

}
