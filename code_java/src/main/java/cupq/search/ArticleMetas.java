package cupq.search;

import cupq.utils.MongoUtil;

import java.util.HashMap;
import java.util.Map;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Projections.fields;
import static com.mongodb.client.model.Projections.include;

public class ArticleMetas {

    private final static String[] ARTICLE_META_FIELDS = new String[] {
            "pub_year",
            "pub_month",
            "pub_day",
            "pub_types",
            "issn",
            "issn_linking",
            "has_clinical_keyword"
    };

    private final static Map<Long, ArticleMeta> ARTICLE_METAS = initArticleMetas();

    private static Map<Long, ArticleMeta> initArticleMetas() {
        var metas = new HashMap<Long, ArticleMeta>();
        MongoUtil.ARTICLES_COLLECTION
                .find()
                .projection(fields(include(ARTICLE_META_FIELDS)))
                .forEach(doc -> {
                    var journal = Journals.getJournal(doc.getString("issn_linking"));
                    if (journal == null) {
                        journal = Journals.getJournal(doc.getString("issn"));
                    }
                    if (journal != null) {
                        metas.put(doc.getInteger("_id").longValue(), new ArticleMeta(doc, journal));
                    }
                });
        return metas;
    }

    public static ArticleMeta getArticleMeta(long pmid) {
        return ARTICLE_METAS.get(pmid);
    }

    public static void updateArticleMetas() {
        MongoUtil.ARTICLES_COLLECTION
                .find(eq("pending", true))
                .projection(fields(include(ARTICLE_META_FIELDS)))
                .forEach(doc -> {
                    var journal = Journals.getJournal(doc.getString("issn_linking"));
                    if (journal == null) {
                        journal = Journals.getJournal(doc.getString("issn"));
                    }
                    if (journal != null) {
                        ARTICLE_METAS.put(doc.getInteger("_id").longValue(), new ArticleMeta(doc, journal));
                    }
                });
    }

}
