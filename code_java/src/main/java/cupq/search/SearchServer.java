package cupq.search;

import cupq.utils.NetworkUtil;
import cupq.utils.TimeUtil;

import java.net.ServerSocket;
import java.util.ArrayList;


public class SearchServer {

    public static void main(String[] args) {
        SearchServer.run();
    }

    public static void run(){

        // Prime search data.
        ArticleMetas.getArticleMeta(0);
        SearchIndex.getSearchResults(new ArrayList<>(0));

        // Notify that updates are complete.
        Updates.notifyUpdatesComplete();

        // Run server update thread in background.
        new SearchServerUpdateThread().start();

        TimeUtil.println("INFO : search server ready");
        try (var serverSocket = new ServerSocket(NetworkUtil.MAIN_PORT)) {
            while (true) {
                new SearchServerQueryThread(serverSocket.accept()).start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
