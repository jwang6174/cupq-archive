package cupq.search;

import java.util.ArrayList;
import java.util.List;

public class Search {

    public static List<Long> getResults(
            List<String> tokens,
            double weightJournalImpactFactor,
            double weightJournalTotalCites,
            double weightDays,
            double weightTokenScore,
            double weightTokenCountTitle,
            double weightClinicalKeyword) {

        // Look through index for possible search results.
        var possibleResults = SearchIndex.getSearchResults(tokens);

        // Process search results and keep the top 500.
        var processedResults = SearchRanking.processSearchResults(
                possibleResults,
                tokens.size(),
                500,
                weightJournalImpactFactor,
                weightJournalTotalCites,
                weightDays,
                weightTokenScore,
                weightTokenCountTitle,
                weightClinicalKeyword);

        // Create list with just search result PMIDs.
        var processedResultPMIDs = new ArrayList<Long>(processedResults.size());
        for (var result : processedResults) {
            processedResultPMIDs.add(result.getPMID());
        }

        return processedResultPMIDs;
    }

}
