package cupq.search;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SearchServerQueryThread extends Thread {

    private final Socket socket;

    public SearchServerQueryThread(Socket socket) {
        super();
        this.socket = socket;
    }

    @Override
    public void run() {
        try (var out = new PrintWriter(socket.getOutputStream(), true);
             var in = new BufferedReader(new InputStreamReader(socket.getInputStream())))
        {
            var line = in.readLine();
            var data = (JSONObject) new JSONParser().parse(line);

            // Query tokens sorted alphabetically.
            var tokenObjects = (JSONArray) data.get("tokens");
            var tokens = new ArrayList<String>(tokenObjects.size());
            for (var obj : tokenObjects) {
                tokens.add((String) obj);
            }
            tokens.sort(String::compareToIgnoreCase);

            // Publication type.
            var resultType = (String) data.get("result_type");

            // Page number.
            var pageNumber = ((Long) data.get("page_number")).intValue();

            // Results per page.
            var resultsPerPage = ((Long) data.get("results_per_page")).intValue();

            // Specialty.
            var specialty = (String) data.get("specialty");

            // Sub-score weights.
            var weightJournalImpactFactor = (double) data.get("weight_journal_impact_factor");
            var weightJournalTotalCites = (double) data.get("weight_journal_total_cites");
            var weightDays = (double) data.get("weight_days");
            var weightTokenScore = (double) data.get("weight_token_score");
            var weightTokenCountTitle = (double) data.get("weight_token_count_title");
            var weightClinicalKeyword = (double) data.get("weight_clinical_keyword");

            // Whether the cache should be used.
            var useCache = (boolean) data.get("use_cache");

            // Check search cache for results that have already been
            // calculated for this query.
            var results = SearchCache.getResults(tokens);

            // Calculate and store search results if the search query
            // is brand new or if the cache should be overridden.
            if (results == null || !useCache) {

                // Use newly calculated results.
                results = Search.getResults(
                        tokens,
                        weightJournalImpactFactor,
                        weightJournalTotalCites,
                        weightDays,
                        weightTokenScore,
                        weightTokenCountTitle,
                        weightClinicalKeyword);

                // Write results to MongoDB for faster lookup the next time
                // someone searches this query.
                if (useCache) {
                    SearchCache.store(
                            results,
                            tokens,
                            weightJournalImpactFactor,
                            weightJournalTotalCites,
                            weightDays,
                            weightTokenScore,
                            weightTokenCountTitle,
                            weightClinicalKeyword);
                }
            }

            // Filter PMIDs by publication type and specialty.
            var filteredPMIDs = new ArrayList<Long>(results.size());
            for (var pmid : results) {
                var articleMeta = ArticleMetas.getArticleMeta(pmid);
                if (articleMeta != null
                        && articleMeta.hasPubType(resultType)
                        && articleMeta.getJournal().hasSpecialty(specialty)) {
                    filteredPMIDs.add(pmid);
                }
            }

            // Get PMIDs for specified page.
            List<Long> pagePMIDs;
            var startIndex = (pageNumber - 1) * resultsPerPage;
            var endIndex = Math.min(pageNumber * resultsPerPage, filteredPMIDs.size());
            if (startIndex < filteredPMIDs.size()) {
                pagePMIDs = filteredPMIDs.subList(startIndex, endIndex);
            } else {
                pagePMIDs = new ArrayList<>(0);
            }

            // Send JSON response, then close socket connection.
            var response = new HashMap<String, Object>();
            response.put("results", pagePMIDs);
            response.put("total_result_count", filteredPMIDs.size());
            out.println(JSONObject.toJSONString(response));
            socket.close();

        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }
}
