package cupq.search;

import cupq.utils.PathUtil;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Journals {

    private static final Map<String, Journal> JOURNALS = initJournals();

    private static Map<String, Journal> initJournals() {
        var journals = new HashMap<String, Journal>();
        try {
            var data = (JSONObject) new JSONParser().parse(new FileReader(PathUtil.MEDICAL_JOURNALS_PATH.toString()));
            for (var key : data.keySet()) {
                var issnLinking = (String) key;
                journals.put(issnLinking, new Journal(issnLinking, (JSONObject) data.get(key)));
            }
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        return Collections.unmodifiableMap(journals);
    }

    public static Journal getJournal(String issnLinking) {
        return JOURNALS.get(issnLinking);
    }

}
