package cupq.search;

import com.mongodb.client.model.UpdateOptions;
import cupq.utils.MongoUtil;
import org.bson.Document;

import java.util.Collections;
import java.util.List;

import static com.mongodb.client.model.Filters.eq;

public class SearchCache {

    public static List<Long> getResults(List<String> tokens) {
        var tokensString = String.join(" ", tokens);
        var cursor = MongoUtil.SEARCH_CACHE_COLLECTION.find(eq("_id", tokensString)).first();
        if (cursor != null) {
            return Collections.unmodifiableList(cursor.getList("results", Long.class));
        } else {
            return null;
        }
    }

    public static void store(
            List<Long> results,
            List<String> tokens,
            double weightJournalImpactFactor,
            double weightJournalTotalCites,
            double weightDays,
            double weightTokenScore,
            double weightTokenCountTitle,
            double weightClinicalKeyword) {

        var tokensString = String.join(" ", tokens);
        var info = new Document()
                .append("results", results)
                .append("tokens", tokens)
                .append("weight_journal_impact_factor", weightJournalImpactFactor)
                .append("weight_journal_total_cites", weightJournalTotalCites)
                .append("weight_days", weightDays)
                .append("weight_token_score", weightTokenScore)
                .append("weight_token_count_title", weightTokenCountTitle)
                .append("weight_clinical_keyword", weightClinicalKeyword);
        var update = new Document("$set", info);
        var updateOptions = new UpdateOptions().upsert(true);
        MongoUtil.SEARCH_CACHE_COLLECTION.updateOne(eq("_id", tokensString), update, updateOptions);
    }

    public static void updateSearchCache() {
        for (var doc : MongoUtil.SEARCH_CACHE_COLLECTION.find()) {
            var results = Search.getResults(
                    doc.getList("tokens", String.class),
                    doc.getDouble("weight_journal_impact_factor"),
                    doc.getDouble("weight_journal_total_cites"),
                    doc.getDouble("weight_days"),
                    doc.getDouble("weight_token_score"),
                    doc.getDouble("weight_token_count_title"),
                    doc.getDouble("weight_clinical_keyword"));
            store(
                    results,
                    doc.getList("tokens", String.class),
                    doc.getDouble("weight_journal_impact_factor"),
                    doc.getDouble("weight_journal_total_cites"),
                    doc.getDouble("weight_days"),
                    doc.getDouble("weight_token_score"),
                    doc.getDouble("weight_token_count_title"),
                    doc.getDouble("weight_clinical_keyword"));
        }
    }

}
