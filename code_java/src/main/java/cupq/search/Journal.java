package cupq.search;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class Journal {

    private final String ISSN_LINKING;
    private final int TOTAL_CITES;
    private final double IMPACT_FACTOR;
    private final Set<String> SPECIALTIES;

    public Journal(String issnLinking, JSONObject data) {

        // Initialize ISSN_LINKING.
        ISSN_LINKING = issnLinking;

        // Initialize TOTAL_CITES.
        int totalCites;
        try {
            totalCites = Integer.parseInt(((String) data.get("total_cites")).replaceAll(",", ""));
        } catch (NumberFormatException e) {
            totalCites = 0;
        }
        TOTAL_CITES = totalCites;

        // Initialize IMPACT_FACTOR.
        var impactFactor = 0d;
        try {
            impactFactor = Double.parseDouble(((String) data.get("impact_factor")).replaceAll(",", ""));
        } catch (NumberFormatException e) {
            impactFactor = 0;
        }
        IMPACT_FACTOR = impactFactor;

        // Initialize SPECIALTIES.
        var specialties = new HashSet<String>();
        for (var obj : (JSONArray) data.get("specialties")) {
            var specialty = (String) obj;
            specialties.add(specialty);
        }
        SPECIALTIES = Collections.unmodifiableSet(specialties);
    }

    public String getISSNLinking() {
        return ISSN_LINKING;
    }

    public int getTotalCites() {
        return TOTAL_CITES;
    }

    public double getImpactFactor() {
        return IMPACT_FACTOR;
    }

    public Set<String> getSpecialties() {
        return SPECIALTIES;
    }

    public boolean hasSpecialty(String specialty) {
        return SPECIALTIES.contains(specialty) || specialty.equalsIgnoreCase("All Specialties");
    }

}
