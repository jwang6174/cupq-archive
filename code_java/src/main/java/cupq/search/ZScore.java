package cupq.search;

import java.util.ArrayList;
import java.util.List;

public class ZScore {

    private final List<Double> VALUES = new ArrayList<>();
    private double mean = 0d;
    private double sd = 0d;

    public void add(double value) {
        VALUES.add(value);
    }

    private void setMean() {
        mean = 0d;
        for (var value : VALUES) {
            mean += value;
        }
        mean /= Math.max(1, VALUES.size());
    }

    private void setSD() {
        for (var value : VALUES) {
            sd += Math.pow((value - mean), 2);
        }
        sd /= Math.max(1, VALUES.size());
        sd = Math.sqrt(sd);
    }

    public void markComplete() {
        setMean();
        setSD();
    }

    public double normalize(double raw) {
        return (raw - mean) / Math.max(1, sd);
    }

}
