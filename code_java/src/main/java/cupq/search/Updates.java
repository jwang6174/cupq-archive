package cupq.search;

import cupq.utils.PathUtil;
import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

public class Updates {

    public static void notifyUpdatesComplete() {
        try (var file = new FileWriter(PathUtil.SEARCH_SERVER_UPDATE_PATH.toString())) {
            var data = new HashMap<String, Boolean>();
            data.put("updates_available", false);
            file.write(new JSONObject(data).toJSONString());
            file.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
