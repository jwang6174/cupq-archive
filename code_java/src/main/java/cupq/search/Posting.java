package cupq.search;

import org.json.simple.JSONObject;

public class Posting {

    private final long PMID;
    private final double SCORE;
    private final boolean IN_TITLE;

    public Posting(JSONObject json) {

        // Initialize PMID.
        PMID = (long) json.get("_id");

        // Initialize SCORE.
        var score = 0d;
        try {
            score = (double) json.get("score");
        } catch (ClassCastException e) {
            score = 0d;
        }
        SCORE = score;

        // Initialize IN_TITLE.
        IN_TITLE = (boolean) json.get("in_title");
    }

    public long getPMID() {
        return PMID;
    }

    public double getScore() {
        return SCORE;
    }

    public boolean inTitle() {
        return IN_TITLE;
    }

}
