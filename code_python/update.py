"""
Update CupQ.
"""

import json

from cupq.pubmed.download import download_pubmed
from cupq.pubmed.migrate import migrate_pubmed
from cupq.nlp.junctions import collect_junctions
from cupq.index.build import build_index
from cupq.utils.timelog import timelog
from cupq.utils.pathutil import SEARCH_SERVER_UPDATE_PATH


def _notify():
    """
    Notify the dedicated search server that updates are available.
    """
    with open(SEARCH_SERVER_UPDATE_PATH, 'w') as f:
        json.dump({'updates_available': True}, f, indent=4)


def update():
    """
    Update CupQ.
    """
    timelog('INFO : starting server update')

    timelog('INFO : downloading PubMed')
    download_pubmed()

    timelog('INFO : migrating PubMed')
    migrate_pubmed()

    timelog('INFO : collecting junction words')
    collect_junctions(fresh=False)

    timelog('INFO : building index')
    build_index(update=True)

    timelog('INFO : finished server update')

    _notify()


if __name__ == '__main__':
    update()
