"""
Set up CupQ from scratch.
"""

from cupq.utils.timelog import timelog

timelog('INFO : starting server initiation')

timelog('INFO : downloading PubMed')
from cupq.pubmed.download import download_pubmed
download_pubmed()

timelog('INFO : migrating PubMed')
from cupq.pubmed.migrate import migrate_pubmed
migrate_pubmed()

timelog('INFO : collecting junction words')
from cupq.nlp.junctions import collect_junctions
collect_junctions(fresh=True)

timelog('INFO : training word embeddings')
from cupq.embedding.train_words import train_from_scratch
train_from_scratch()

timelog('INFO : building index')
from cupq.index.build import build_index
build_index(update=False)

timelog('INFO : resetting pending articles')
from cupq.utils.dbutil import ARTICLES_COLLECTION
ARTICLES_COLLECTION.update_many({'pending': True}, {'$set': {'pending': False}})

timelog('INFO : finished server initiation')
