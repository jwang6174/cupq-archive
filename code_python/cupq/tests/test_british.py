import unittest

from cupq.nlp.british import british_normalize


class BritishTest(unittest.TestCase):
    def test_normalize(self):
        self.assertEqual(british_normalize('flavour'), 'flavor')
        self.assertEqual(british_normalize('ischaemic'), 'ischemic')
        self.assertEqual(british_normalize('analyse'), 'analyze')


if __name__ == '__main__':
    unittest.main()
