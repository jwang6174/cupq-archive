import unittest

from cupq.nlp.tokens import get_tokens


class TokensTest(unittest.TestCase):
    def test_get_tokens(self):
        self.assertEqual(get_tokens('You ARE a pain'), ['are', 'pain'])
        self.assertEqual(get_tokens('you are a pain.'), ['pain'])
        self.assertEqual(get_tokens('Covid19 is a pain.'), ['covid', '19', 'pain'])
        self.assertEqual(get_tokens('US should not withdraw from WHO'), ['u', 's', 'withdraw', 'who'])
        self.assertEqual(get_tokens('A Vitamin A is awesome'), ['vitamin', 'a', 'awesome'])
        self.assertEqual(get_tokens('intra-arterial/intravenous'), ['intra', 'arterial', 'intra', 'venou'])
        self.assertEqual(get_tokens('pediatrics investigator'), ['pediatric', 'investigator'])
        self.assertEqual(get_tokens("BCG's mechanism of action"), ['bcg', 'mechanism', 'action'])
        self.assertEqual(get_tokens("Alzheimer's disease"), ['alzheimer', 'disease'])
        self.assertEqual(get_tokens('Alzheimers disease'), ['alzheimer', 'disease'])
        self.assertEqual(get_tokens('pediatrics'), ['pediatric'])
        self.assertEqual(get_tokens('orthopaedic surgery'), ['orthopedic', 'surgery'])


if __name__ == '__main__':
    unittest.main()
