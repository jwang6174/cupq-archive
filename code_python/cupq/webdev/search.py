"""
Get search results by querying a dedicated search server.

Attributes:
    _HOST (str): Search server IP address.
    _PORT (int): Search server port number.
    _BUFFER_SIZE (int): Response buffer size.
"""

import json
from socket import socket, AF_INET, SOCK_STREAM

from cupq.nlp.tokens import get_tokens

_HOST = 'localhost'
_PORT = 1234
_BUFFER_SIZE = 1024


def get_search_response(search_text, specialty, result_type, page_number, results_per_page, use_cache):
    """
    Args:
        search_text (str): Search string.
        specialty (str): Medical specialty.
        result_type (str): Publication type.
        page_number (int): Page number.
        results_per_page (int): Number of results per page.
        use_cache (bool): True if cache should be overridden.

    Returns:
        (dict): JSON response containing PMIDs of article results and
            total number of results for pagination.
    """

    # Truncate search text if too long.
    search_text = search_text[:100]

    # Create message to send to search server.
    message = json.dumps({
        'tokens': get_tokens(search_text),
        'page_number': page_number,
        'result_type': result_type,
        'results_per_page': results_per_page,
        'specialty': specialty,
        "weight_journal_impact_factor": 2.0,
        "weight_journal_total_cites": 2.0,
        "weight_days": 4.0,
        "weight_token_score": 3.0,
        "weight_token_count_title": 3.0,
        "weight_clinical_keyword": 1.0,
        "use_cache": use_cache
    })

    # Open connection to server and send message.
    client = socket(AF_INET, SOCK_STREAM)
    client.connect((_HOST, _PORT))
    client.sendall((message + '\n').encode())

    # Receive response from server.
    response = ''
    while True:
        chunk = client.recv(_BUFFER_SIZE).decode('utf-8', 'ignore')
        if not chunk:
            break
        response += chunk

    # Close connection to server.
    client.close()

    # Return JSON response.
    return json.loads(response) if response else {}
