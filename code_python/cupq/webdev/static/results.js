$(document).ready(function() {

    // Make body visible, avoid flicker
   	$("body").css("visibility", "visible");

	$("#search-btn").click(function() {
		$("#search-form").submit();
	});

});