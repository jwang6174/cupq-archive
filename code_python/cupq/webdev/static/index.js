$(document).ready(function() {

	// Initialize top space height
	setTopSpace();

	// Resize height of top spze
	$(window).resize(function() {
		setTopSpace();
	});

	// Set height of top space
	function setTopSpace() {
		$(".top-space").height($(window).height() / 12);
	}

	// Submit form when search button is clicked
	$("#search-btn").click(function() {
		$("#search-form").submit();
	});

});

$(window).on("load", function() {
	// Make logo and body visible after window is loaded
	$("#logo").css("visibility", "visible");
	$("body").css("visibility", "visible");
	$("#search-input").focus();
});