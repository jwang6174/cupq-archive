"""
Perform spell check.

Attributes:
    _SUBSCRIPTION_KEY (str): Bing spell check API key.
    _HEADERS (dict of str, str): Request headers.
"""

import json
import http.client
import urllib.request
import urllib.parse

from cupq.utils.pathutil import SPELLCHECK_API_KEY_PATH

with open(SPELLCHECK_API_KEY_PATH, 'r') as f:
    _SUBSCRIPTION_KEY = json.load(f)['key']

_HEADERS = {
    'Ocp-Apim-Subscription-Key': _SUBSCRIPTION_KEY
}


def get_spellcheck_suggestion(s):
    """
    Args:
        s (str): Some string.

    Returns:
        (str): Spell check suggestion.
    """
    parameters = urllib.parse.urlencode({
        'text': s,
        'mode': 'spell'
    })
    suggestion = s
    connection = http.client.HTTPSConnection('api.cognitive.microsoft.com')
    connection.request('GET', f'/bing/v7.0/spellcheck/?{parameters}', '{body}', _HEADERS)
    response = connection.getresponse()
    data = json.loads(response.read())
    for replacement in data['flaggedTokens']:
        suggestion = suggestion.replace(replacement['token'], replacement['suggestions'][0]['suggestion'])
    connection.close()
    return suggestion
