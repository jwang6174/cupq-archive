from traceback import format_exc
from time import strftime
from titlecase import titlecase
from calendar import month_abbr

from flask import Flask, render_template, request
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address

from cupq.jcr.medical import get_specialties
from cupq.webdev.search import get_search_response
from cupq.webdev.pagination import Pagination
from cupq.webdev.spellcheck import get_spellcheck_suggestion
from cupq.utils.dbutil import SEARCH_LOG_COLLECTION, ARTICLES_COLLECTION

app = Flask(__name__)

limiter = Limiter(app, key_func=get_remote_address)

RESULTS_PER_PAGE = 10

RESULT_TYPES = ['All Types', 'Reviews', 'Guidelines', 'RCTs']

SPECIALTIES = ['All Specialties'] + get_specialties()


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/beta')
def beta():
    return render_template('beta.html')


@app.route('/search', methods=['GET'])
@limiter.limit('5/second')
def search():
    log = {
        'time': strftime('%c'),
        'search_text': None,
        'page_number': None,
        'result_type': None,
        'specialty': None,
        'ip_address': None,
        'got_search_response': False,
        'got_result_info': False,
        'got_pagination': False,
        'spellcheck_successful': False,
        'spellcheck_exception': None,
        'spellcheck_traceback': None,
        'search_successful': False,
        'search_exception': None,
        'search_traceback': None,
    }

    try:
        search_text = request.args.get('q', type=str)
        log['search_text'] = search_text

        page_number = request.args.get('p', type=int)
        log['page_number'] = page_number

        result_type = request.args.get('t', type=str)
        log['result_type'] = result_type

        specialty = request.args.get('s', type=str)
        log['specialty'] = specialty

        ip_address = request.environ['REMOTE_ADDR']
        log['ip_address'] = ip_address

        search_response = get_search_response(search_text,
                                              specialty,
                                              result_type,
                                              page_number,
                                              RESULTS_PER_PAGE,
                                              False)
        log['got_search_response'] = True

        result_info = _get_result_info(search_response['results'])
        log['got_result_info'] = True

        pagination = Pagination(search_response['total_result_count'], page_number, RESULTS_PER_PAGE)
        log['got_pagination'] = True

        try:
            spellcheck_suggestion = get_spellcheck_suggestion(search_text)
            has_spellcheck_suggestion = True if spellcheck_suggestion != search_text else False
            log['spellcheck_successful'] = True

        except Exception as e:
            spellcheck_suggestion = ''
            has_spellcheck_suggestion = False
            log['spellcheck_successful'] = False
            log['spellcheck_exception'] = str(e)
            log['spellcheck_traceback'] = format_exc()

        log['search_successful'] = True

        template = render_template(
            'results.html',
            search_text=search_text,
            page_number=page_number,
            pagination=pagination,
            result_type=result_type,
            result_type_options=RESULT_TYPES,
            specialty=specialty,
            specialty_options=SPECIALTIES,
            has_spellcheck_suggestion=has_spellcheck_suggestion,
            spellcheck_suggestion=spellcheck_suggestion,
            result_info=result_info)

        SEARCH_LOG_COLLECTION.insert_one(log)
        return template

    except Exception as e:
        log['search_successful'] = False
        log['search_exception'] = str(e)
        log['search_traceback'] = format_exc()
        SEARCH_LOG_COLLECTION.insert_one(log)
        return render_template('oops.html')


def _get_result_info(results):
    """
    Args:
        results (list of int): Result PMIDs.

    Returns:
        (list of dict): Result information for display.
    """
    info = []
    for i, pmid in enumerate(results):
        doc = ARTICLES_COLLECTION.find_one({'_id': pmid})
        try:
            month = month_abbr[int(doc['pub_month'])]
        except (TypeError, ValueError):
            month = doc['pub_month']
        info.append({
            'index': i,
            'title': doc['title'],
            'abstract': _truncate_by_word_count(doc['abstract'], 100),
            'journal': _titlecase(doc['journal']),
            'date': f"{month} {doc['pub_year']}",
            'link': f"https://doi.org/{doc['doi']}"
        })
    return info


def _truncate_by_word_count(s, n):
    """
    Args:
        s (str): Some string.
        n (int): Maximum word count.

    Returns:
        (str): Truncated string.
    """
    words = s.split()
    mod = ' '.join(words[:n])
    if len(words) > n:
        mod += ' ...'
    return mod


def _titlecase(s):
    """
    Args:
        s (str): Some string.

    Returns:
        (str): Titlecase string accounting for fully capitalized words.
    """
    if s.isupper():
        return s
    else:
        return titlecase(s)


if __name__ == '__main__':
    app.run(debug=True)
