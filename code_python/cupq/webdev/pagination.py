from math import ceil


class Link:
    def __init__(self, number, is_current):
        self._number = number
        self._is_current = is_current

    @property
    def number(self):
        return self._number

    @property
    def is_current(self):
        return self._is_current


class Pagination:
    def __init__(self, total_result_count, current_page, per_page):
        self._total_result_count = total_result_count
        self._current_page = current_page
        self._per_page = per_page
        self._has_previous = self._check_previous()
        self._has_next = self._check_next()
        self._end_page = self._get_end_page()
        self._start_page = self._get_start_page()
        self._links = self._get_links()
        self._has_links = self._check_links()

    @property
    def total_result_count(self):
        return self._total_result_count

    @property
    def current_page(self):
        return self._current_page

    @property
    def per_page(self):
        return self._per_page

    @property
    def has_previous(self):
        return self._has_previous

    @property
    def has_next(self):
        return self._has_next

    @property
    def links(self):
        return self._links

    @property
    def has_links(self):
        return self._has_links

    @property
    def end_page(self):
        return self._end_page

    @property
    def start_page(self):
        return self._start_page

    def _check_previous(self):
        return self.total_result_count > self.per_page and self.current_page > 1

    def _check_next(self):
        return self.current_page * self.per_page < self.total_result_count

    def _get_start_page(self):
        start_page = self.end_page - 9
        if start_page < 1:
            start_page = 1
        return start_page

    def _get_end_page(self):
        end_page = int(ceil(self.current_page / 10)) * 10
        if end_page * self.per_page > self.total_result_count:
            end_page = int(ceil(self.total_result_count / self.per_page))
        return end_page

    def _get_links(self):
        links = []
        for page in range(self.start_page, self.end_page + 1):
            links.append(Link(page, page == self.current_page))
        return links

    def _check_links(self):
        return len(self.links) > 1
