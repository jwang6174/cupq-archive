"""
Utility module for paths.

Attributes:
    ROOT_PATH (str): Root directory.
    DATA_PATH (str): Data directory.
    PUBMED_PATH (str): PubMed directory.
    PUBMED_MIGRATE_LOG_PATH (str): Used to resume migration after
        unexpected interruptions.
    JCR_PATH (str): JCR directory.
    WORD_EMBEDDINGS_PATH (str): Contains word embeddings.
    JUNCTION_WORDS_PATH (str): Contains all hyphenated words in PubMed.
    TOKEN_ARTICLE_COUNTS_PATH (str): Contains the number of articles
        containing each token.
    MEDICAL_JOURNALS_PATH (str): Contains journal ISSNs and the
        specialties that they belong to.
    NEW_TOKENS_PATH (str): Contains tokens that were recently updated.
    SEARCH_SERVER_UPDATE_PATH (str): Used to notify the search server
        that updates are available.
    SPELLCHECK_API_KEY_PATH (str): Contains spellcheck API key.
"""

import os

ROOT_PATH = os.path.join('/', 'home', 'jessewang')
os.makedirs(ROOT_PATH, exist_ok=True)

DATA_PATH = os.path.join(ROOT_PATH, 'data', 'cupq')
os.makedirs(DATA_PATH, exist_ok=True)

PUBMED_PATH = os.path.join(DATA_PATH, 'pubmed')
os.makedirs(PUBMED_PATH, exist_ok=True)

JCR_PATH = os.path.join(DATA_PATH, 'jcr')
os.makedirs(JCR_PATH, exist_ok=True)

PUBMED_MIGRATE_LOG_PATH = os.path.join(DATA_PATH, 'pubmed_migrate_log.txt')

WORD_EMBEDDINGS_PATH = os.path.join(DATA_PATH, 'word_embeddings.model')

JUNCTION_WORDS_PATH = os.path.join(DATA_PATH, 'junction_words.json')

TOKEN_ARTICLE_COUNTS_PATH = os.path.join(DATA_PATH, 'token_article_counts.json')

MEDICAL_JOURNALS_PATH = os.path.join(DATA_PATH, 'medical_journals.json')

NEW_TOKENS_PATH = os.path.join(DATA_PATH, 'new_tokens.json')

SEARCH_SERVER_UPDATE_PATH = os.path.join(DATA_PATH, 'search_server_update.json')

SPELLCHECK_API_KEY_PATH = os.path.join(DATA_PATH, 'spellcheck_api_key.json')
