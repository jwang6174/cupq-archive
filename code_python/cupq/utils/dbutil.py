"""
Utility module for databases.

Attributes:
    MONGO_CLIENT (obj): Mongo client that maintains a connection pool.
        Use this client whenever a connection is required.
    CUPQ_DB (Database): CupQ database.
    CUPQ_FS (GridFS): CupQ GridFS instance.
    ARTICLES_COLLECTION (Collection): Articles collection.
    SEARCH_LOG_COLLECTION (Collection): Search log collection.
"""

from pymongo import MongoClient
from gridfs import GridFS

MONGO_CLIENT = MongoClient('127.0.0.1')
CUPQ_DB = MONGO_CLIENT['cupq']
CUPQ_FS = GridFS(CUPQ_DB)
ARTICLES_COLLECTION = CUPQ_DB['articles']
SEARCH_LOG_COLLECTION = CUPQ_DB['search_log']
