"""
Print messages with timestamps.
"""

from time import strftime


def timelog(message):
    """
    Args:
        message (str): Print this message with a timestamp.
    """
    print('%s : %s' % (strftime('%c'), message))
