"""
Utility module for working with strings.

Attributes:
    _STOPWORDS (set of str): English stopwords.
"""

from nltk.corpus import stopwords

_STOPWORDS = stopwords.words('english')


def remove_symbols(s):
    """
    Args:
        s (str): Some string.

    Returns:
        (str): String with only alphanumeric characters and spaces.
    """
    modified = ''
    for c in s:
        if c.isalnum() or c.isspace():
            modified += c
    return ' '.join(modified.split())


def has_caps_in_context(child, parent):
    """
    Args:
        child (str): A substring of `parent`.
        parent (str): Entire text containing `child`.

    Returns:
        (bool): True if `child` has multiple capital letters and
            `parent` is not all capitalized.
    """
    return ((len(child) > 1 and count_char_caps(child) > 1) or (len(child) == 1 and child.isupper())
            and not parent.isupper())


def count_char_caps(s):
    """
    Args:
        s (str): Some string.

    Returns:
        (int): Number of capitalized characters in `s`.
    """
    return [c.isupper() for c in s].count(True)


def is_stopword(s):
    """
    Args:
        s (str): Some string.

    Returns:
            (bool): True if `s` is a stopword.
    """
    return s.lower() in _STOPWORDS


def pseudo_copy_case(source, copier):
    """
    Args:
        source (str): The string that `copier` should pseudo-copy case.
        copier (str): Pseudo-copy case this string compared to `source`.

    Returns:
        (str) `copier` with pseudo-copy case compared to `source`.
    """
    if count_char_caps(source) > 1:
        return copier.upper()
    else:
        return copier.lower()
