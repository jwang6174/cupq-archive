"""
Get index metadata for a PubMed article.

Attributes:
    _WORD_EMBEDDINGS (Word2Vec): Word embeddings.
    _TokenMetadata (namedtuple): Stores token metadata for indexing.
"""

import numpy as np
from gensim.models import Word2Vec
from scipy.spatial.distance import cosine
from collections import namedtuple

from cupq.nlp.tokens import get_tokens
from cupq.embedding.train_words import VECTOR_SIZE
from cupq.utils.pathutil import WORD_EMBEDDINGS_PATH

_WORD_EMBEDDINGS = Word2Vec.load(WORD_EMBEDDINGS_PATH)

_TokenMetadata = namedtuple('TokenMetadata', ['token', 'score', 'in_title'])

# Make sure numpy warnings are raised as errors.
np.seterr('raise')


def _get_embedding(token):
    """
    Args:
        token (str): A token.

    Returns:
        (ndarray): Token embedding.
    """
    if token in _WORD_EMBEDDINGS.wv:
        return _WORD_EMBEDDINGS.wv[token]
    else:
        return np.zeros(VECTOR_SIZE)


def _get_body_embedding(tokens):
    """
    Args:
        tokens (list of str): Tokens

    Returns:
        (ndarray): Body embedding.
    """
    embedding = np.zeros(VECTOR_SIZE)
    if tokens:
        for token in tokens:
            embedding += _get_embedding(token)
        embedding = np.divide(embedding, len(tokens))
    return embedding


def _get_token_scores(all_tokens, title_tokens):
    """
    Args:
        all_tokens (frozenset of str): All tokens.
        title_tokens (list of str): Title tokens.

    Returns:
        (dict of str, float): Token scores. Calculated by comparing the
            embedding of each token to the article title embedding.
    """
    scores = {}
    title_embedding = _get_body_embedding(title_tokens)
    for token in all_tokens:
        token_embedding = _get_embedding(token)
        if np.any(token_embedding) and np.any(title_embedding):
            scores[token] = 1 - cosine(token_embedding, title_embedding)
        else:
            scores[token] = 0
    return scores


def get_article_metadata(article):
    """
    Args:
        article (dict): PubMed article fetched from MongoDB.

    Returns:
        (list of _TokenMetadata): Article metadata for indexing.
    """
    article_metadata = []
    title_tokens = get_tokens(article['title'])
    abstract_tokens = get_tokens(article['abstract'])
    all_tokens = frozenset(title_tokens + abstract_tokens)
    token_scores = _get_token_scores(all_tokens, title_tokens)
    for token in all_tokens:
        score = token_scores[token]
        in_title = token in title_tokens
        article_metadata.append(_TokenMetadata(token, score, in_title))
    return article_metadata
