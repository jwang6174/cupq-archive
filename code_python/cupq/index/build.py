"""
Build inverted index.
"""

import json

from cupq.index.article import get_article_metadata
from cupq.utils.dbutil import CUPQ_FS, ARTICLES_COLLECTION
from cupq.utils.pathutil import NEW_TOKENS_PATH
from cupq.utils.timelog import timelog


def _get_stored_postings(token):
    """
    Args:
        token (str): Normalized search term.

    Returns:
        (list of dict): Token index information.
    """
    return json.loads(CUPQ_FS.get(token).read()) if CUPQ_FS.exists(token) else []


def _set_postings(token, postings):
    """
    Set postings for token.

    Args:
        token (str): Normalized search term.
        postings (list of dict): Postings.
    """
    if CUPQ_FS.exists(token):
        CUPQ_FS.delete(token)
    CUPQ_FS.put(json.dumps(postings), _id=token, encoding='utf8')


def _merge_postings(old_postings, new_postings):
    """
    Args:
        old_postings (list of dict): Postings that were already stored,
            sorted by PMID in ascending order.
        new_postings (list of dict): Postings that need to be added,
            sorted by PMID in ascending order.

    Returns:
        (list of dict): Merged old and new postings sorted by PMID in
            ascending order.
    """
    merge_postings = []
    old_postings_index = 0
    new_postings_index = 0
    while new_postings_index < len(new_postings):
        while old_postings_index < len(old_postings):
            old_posting = old_postings[old_postings_index]
            if new_postings_index < len(new_postings):
                new_posting = new_postings[new_postings_index]
                if new_posting['_id'] == old_posting['_id']:
                    merge_postings.append(new_posting)
                    new_postings_index += 1
                    old_postings_index += 1
                elif new_posting['_id'] < old_posting['_id']:
                    merge_postings.append(new_posting)
                    new_postings_index += 1
                else:
                    merge_postings.append(old_posting)
                    old_postings_index += 1
            else:
                merge_postings.append(old_posting)
                old_postings_index += 1
        if new_postings_index < len(new_postings):
            new_posting = new_postings[new_postings_index]
            merge_postings.append(new_posting)
        new_postings_index += 1
    return merge_postings


def _get_index(start_id, batch_size):
    """
    Args:
        start_id (int): Starting article ID.
        batch_size (int): Number of articles to process per batch.

    Returns:
        (dict): Key is token and value is list of dict, where each
            value is article information for that token.
        (int): ID of last article processed.
    """
    index = {}
    last_article_id = 0
    articles = (ARTICLES_COLLECTION
                .find({'_id': {'$gt': start_id}, 'pending': True}, {'title': 1, 'abstract': 1})
                .sort('_id', 1)
                .limit(batch_size))

    # Build index.
    for article in articles:
        article_metadata = get_article_metadata(article)
        for token_metadata in article_metadata:
            index.setdefault(token_metadata.token, []).append({
                '_id': article['_id'],
                'score': token_metadata.score,
                'in_title': token_metadata.in_title
            })

        # Update last article ID.
        last_article_id = article['_id']

    return index, last_article_id


def build_index(update):
    """
    Build index.

    Args:
        update (bool): True if new tokens should be written to a file.
    """
    count = 0
    start_id = 0
    batch_size = 10_000
    all_tokens = set()
    index, last_article_id = _get_index(start_id, batch_size)
    while index:
        for token, new_postings in index.items():
            old_postings = _get_stored_postings(token)
            all_postings = _merge_postings(old_postings, new_postings)
            _set_postings(token, all_postings)
            if update:
                all_tokens.add(token)
        count += batch_size
        timelog(f'INFO : indexed {count} articles, last PMID: {last_article_id}')
        index, last_article_id = _get_index(last_article_id, batch_size)
    if update:
        with open(NEW_TOKENS_PATH, 'w') as f:
            json.dump(list(all_tokens), f, indent=4)
        timelog(f'INFO : updated {len(all_tokens)} tokens')


if __name__ == '__main__':
    build_index(update=False)
