"""
Handles JCR information labeled with medical specialty.

Attributes:
    _SPECIALTIES (dict of str, str): Used to identify medical
        specialty based on JCR filename.
"""

import os
import csv
import json

from cupq.jcr.journal import Journal
from cupq.utils.pathutil import JCR_PATH, MEDICAL_JOURNALS_PATH

_SPECIALTIES = {
    'allergy.csv': 'Allergy & Immunology',
    'anesthesiology.csv': 'Anesthesiology',
    'cardiac-and-cardiovascular-systems.csv': 'Cardiology',
    'clinical-neurology.csv': 'Neurology',
    'critical-care-medicine.csv': 'Critical Care',
    'dermatology.csv': 'Dermatology',
    'emergency-medicine.csv': 'Emergency Medicine',
    'gastroenterology-and-hepatology.csv': 'Gastroenterology & Hepatology',
    'general-and-internal-medicine.csv': 'Internal Medicine',
    'geriatrics-and-gerontology.csv': 'Geriatrics',
    'immunology.csv': 'Allergy & Immunology',
    'infectious-diseases.csv': 'Infectious Disease',
    'nephrology.csv': 'Nephrology',
    'obstetrics-and-gynecology.csv': 'Obstetrics & Gynecology',
    'opthalmology.csv': 'Opthalmology',
    'orthopedics.csv': 'Orthopedics',
    'otorhinolaryngology.csv': 'Otorhinolaryngology',
    'pathology.csv': 'Pathology',
    'pediatrics.csv': 'Pediatrics',
    'peripheral-vascular-disease.csv': 'Cardiology',
    'primary-health-care.csv': 'Family Medicine',
    'psychiatry.csv': 'Psychiatry',
    'radiology.csv': 'Radiology',
    'rehabilitation.csv': 'Physical Medicine & Rehabilitation',
    'rheumatology.csv': 'Rheumatology',
    'surgery.csv': 'Surgery',
    'urology.csv': 'Urology'
}


def get_medical_journals():
    """
    Returns:
        (dict of str, Journal): Dictionary of journals keyed by linking
            ISSN.
    """
    journals = {}
    for filename in os.listdir(JCR_PATH):
        specialty = _SPECIALTIES[filename]
        path = os.path.join(JCR_PATH, filename)
        with open(path, 'r') as f:
            for row in csv.DictReader(f):
                if row['ISSN']:
                    default_journal = Journal(row['ISSN'], row['Total Cites'], row['Journal Impact Factor'])
                    journals.setdefault(row['ISSN'], default_journal).specialties.add(specialty)
    return journals


def save_medical_journals():
    """
    Get medical journals and save to a JSON file.
    """
    json_data = {}
    for issn, journal in get_medical_journals().items():
        json_data[issn] = {
            'total_cites': journal.total_cites,
            'impact_factor': journal.impact_factor,
            'specialties': list(journal.specialties)
        }
    with open(MEDICAL_JOURNALS_PATH, 'w') as f:
        json.dump(json_data, f, indent=4)


def get_specialties():
    """
    Returns:
        (list of str): Specialties sorted in alphabetical order.
    """
    specialties = list(set(_SPECIALTIES.values()))
    specialties.sort()
    return specialties


if __name__ == '__main__':
    save_medical_journals()
