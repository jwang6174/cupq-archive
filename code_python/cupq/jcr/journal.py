"""
This module contains the `Journal` class.
"""


class Journal:
    """
    Args:
        issn (str): Linking ISSN.
        total_cites (str): Total citations.
        impact_factor (str): Impact factor.

    Attributes:
        _issn (str): Linking ISSN.
        _total_cites (str): Total citations.
        _impact_factor (str): Impact factor.
        _specialties (set of str): Medical specialties.
    """
    def __init__(self, issn, total_cites, impact_factor):
        if issn == '****-****':
            raise ValueError(f'Invalid ISSN: {issn}')
        self._issn = issn
        self._total_cites = total_cites
        self._impact_factor = impact_factor
        self._specialties = set()

    @property
    def issn(self):
        """
        Returns:
            (str): Linking ISSN.
        """
        return self._issn

    @property
    def total_cites(self):
        """
        Returns:
            (str): Total citations.
        """
        return self._total_cites

    @property
    def impact_factor(self):
        """
        Returns:
            (str): Impact factor.
        """
        return self._impact_factor

    @property
    def specialties(self):
        """
        Returns:
            (set of str): Medical specialties.
        """
        return self._specialties
