"""
Normalize junction words.

Attributes:
    _EN_DASH (str): Unicode string for en dash.
    _JUNCTIONS (list of str): Junction characters.
"""

import json
from collections import Counter

from cupq.utils.dbutil import ARTICLES_COLLECTION
from cupq.utils.pathutil import JUNCTION_WORDS_PATH
from cupq.utils.stringutil import remove_symbols

_EN_DASH = u'\u2013'

_JUNCTIONS = [_EN_DASH, '-', '/']


def _get_new_junction_words():
    """
    Returns:
        (dict of str, int): Junction words from pending articles and
            their article counts.
    """
    junction_words = {}
    for article in ARTICLES_COLLECTION.find({'pending': True}):
        article_words = article['title'].split() + article['abstract'].split()
        for word in article_words:
            if _has_junction(word) and any(c.isalpha() for c in word):
                word = word.lower()
                word = replace_junctions_with_spaces(word)
                word = remove_symbols(word)
                if word:
                    junction_words[word] = junction_words.get(word, 0) + 1
    return junction_words


def _get_old_junction_words():
    """
    Returns:
        (dict of str, int): Junction words that have already been
            collected and their article counts.
    """
    with open(JUNCTION_WORDS_PATH, 'a+', encoding='utf-8') as f:
        try:
            f.seek(0)
            return json.load(f)
        except json.JSONDecodeError:
            return {}


def _has_junction(s):
    """
    Args:
        s (str): Some string.

    Returns:
        (bool): True if `s` contains a junction character.
    """
    for junction in _JUNCTIONS:
        if junction in s and not s.startswith(junction) and not s.endswith(junction):
            return True
    return False


def replace_junctions_with_spaces(s):
    """
    Args:
        s (str): Some string.

    Returns:
        (str): `s` with junctions replaced with spaces.
    """
    for junction in _JUNCTIONS:
        s = s.replace(junction, ' ')
    return ' '.join(s.split())


def get_pseudo_junction_words():
    """
    Returns:
        (dict of str, str): Key is a junction word without space. Value
            is junction word with space. All words are in lowercase
            form.
    """
    words = {}
    for space_version, count in _get_old_junction_words().items():
        if count > 10:
            empty_version = space_version.replace(' ', '')
            if empty_version in words and space_version.count(' ') < words[empty_version].count(' '):
                words[empty_version] = space_version
            elif empty_version not in words:
                words[empty_version] = space_version
    return words


def collect_junctions(fresh):
    """
    Collect junctions words and save to file.

    Args:
        fresh (bool): True if old junctions should be erased.
    """
    old_junction_words = _get_old_junction_words() if not fresh else {}
    new_junction_words = _get_new_junction_words()
    all_junction_words = Counter(old_junction_words) + Counter(new_junction_words)
    with open(JUNCTION_WORDS_PATH, 'w', encoding='utf-8') as f:
        json.dump(all_junction_words, f, indent=4)


if __name__ == '__main__':
    collect_junctions(fresh=True)
