"""
Handles text tokenization.

Attributes:
    _LEMMATIZER (WordNetLemmatizer): For lemmatizing words.
    _JUNCTION_WORDS (dict of str, str): Key is a junction word without
        space. Value is junction word with space. All words are in
        lowercase form.
"""

from nltk import word_tokenize
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import sent_tokenize

from cupq.nlp.british import british_normalize
from cupq.nlp.junctions import get_pseudo_junction_words, replace_junctions_with_spaces
from cupq.utils.stringutil import remove_symbols, is_stopword, has_caps_in_context, pseudo_copy_case

_LEMMATIZER = WordNetLemmatizer()

_JUNCTION_WORDS = get_pseudo_junction_words()


def get_tokens(s):
    """
    Args:
        s (str): Some string.

    Returns:
        (list of str): Lemmatized tokens with pseudo-junction words
            replaced with their spaced versions.
    """
    tokens = []
    for sent in sent_tokenize(s):
        prep_sent = pre_process(sent)
        for i, word in enumerate(word_tokenize(prep_sent)):
            if (i == 0 and word.lower() == 'a') or (is_stopword(word) and not has_caps_in_context(word, s)):
                continue
            word = word.lower()
            word = british_normalize(word)
            word = _LEMMATIZER.lemmatize(word)
            word = _strip_suffix_s(word)
            if word:
                tokens.append(word)
    return tokens


def _strip_suffix_s(text):
    """
    Args:
        text (str): Some string.

    Returns:
        `text` without suffix s or apostrophe.
    """
    if len(text) > 2:
        text = text.rstrip("'s")
        text = text.rstrip("s'")
        text = text.rstrip("s")
    return text


def _replace_pseudo_junctions(s):
    """
    Args:
        s (str): Some string.

    Returns:
        (str): `s` with pseudo-junctions replaced with their spaced
            versions.
    """
    mod_words = []
    for word in word_tokenize(s):
        sub_words = _JUNCTION_WORDS.get(word.lower(), '').split()
        if sub_words:
            for sub_word in sub_words:
                sub_word = pseudo_copy_case(word, sub_word)
                mod_words.append(sub_word)
        else:
            mod_words.append(word)
    mod_s = ' '.join(mod_words)
    if mod_s != s:
        return _replace_pseudo_junctions(mod_s)
    else:
        return mod_s


def pre_process(s):
    """
    Args:
        s (str): Some string.

    Returns:
        (str): Pre-processed string.
    """
    s = replace_junctions_with_spaces(s)
    s = remove_symbols(s)
    s = _replace_pseudo_junctions(s)
    s = ' '.join(s.split())
    return s
