"""
Normalize British spelling. This implementation accounts for -our, -re,
-ae, -oe, -ise, -isation, -yse, -ogue, and 'e' when adding suffixes.
This implementation does not account for -ce, -xion, hard versus soft
'c', double characters, and other exceptions. See links below.

Notes:
    https://en.wikipedia.org/wiki/American_and_British_English_spelling_differences#-our,_-or
    https://examples.yourdictionary.com/list-of-suffixes-and-suffix-examples.html

Attributes:
    _SUFFIXES (list of str): Common suffixes.
"""

_SUFFIXES = [
    'eer',
    'er',
    'ion',
    'ity',
    'ment',
    'ness',
    'or',
    'sion',
    'ship',
    'th',
    'able',
    'ible',
    'al',
    'ant',
    'ary',
    'ful',
    'ic',
    'ious',
    'ous',
    'ive',
    'less',
    'y',
    'ed',
    'en',
    'er',
    'ing',
    'ize',
    'ise',
    'ly',
    'ward',
    'wise'
]


def _normalize_our(s):
    """
    Most words ending in an unstressed -our in British English (e.g.
    'colour', 'flavour', 'behaviour', 'harbour', 'humour', 'labour',
    'neighbour', 'rumour', 'splendour') end in -or in American English
    ('color', 'flavor', 'behavior', 'harbor', 'honor', 'humor', 'labor',
    'neighbor', 'rumor', 'splendor'). Wherever the vowel is unreduced
    in pronunciation, e.g., 'contour', 'velour', 'paramour', and
    'troubadour', the spelling is consistent everywhere.

    Notes:
        This implementation does not identify unreduced vowels. For
        example, 'contour' is normalized to 'contour'. However, this is
        okay since it will still normalize British and American
        spellings to the same form.

    Args:
        s (str): Some lowercase string.

    Returns:
        (str): `s` with -our ending normalized to -or.
    """
    if s != 'our' and s.endswith('our'):
        s = s[:-3]
        s += 'or'
    return s


def _normalize_re(s):
    """
    In British English, some words from French, Latin, or Greek end
    with a consonant followed by an unstressed -re. In modern American
    English, most of these words have the ending -er. The difference is
    most common for words ending in -bre or -tre. British spellings
    'calibre', 'centre', 'fibre', 'goitre', 'litre', 'lustre',
    'manoeuvre', 'meagre', and 'metre' all have -er in the American
    spelling.

    Args:
        s (str): Some lowercase string.

    Returns:
        (str): `s` with -re endings normalized to -er in certain cases.
    """
    if s.endswith('bre') or s.endswith('tre'):
        s = s[:-2]
        s += 'er'
    return s


def _normalize_ae_oe(s):
    """
    Many words, especially medical words, that are written with -ae or
    -oe in British English are written with just an -e in American
    English.

    Notes:
        Although 'aesthetics' is more common in American English than
        'esthetics', both American spellings and British spellings will
        be normalized to the same form.

    Args:
        s (str): Some lowercase string.

    Returns:
        (str): `s` with -ae and -oe normalized to -e.
    """
    return s.lower().replace('ae', 'e').replace('oe', 'e')


def _normalize_ise_isation(s):
    """
    American spelling avoids -ise endings in words like 'organize',
    'realize', and 'recognize'. Similarly, 'organization' is used
    in American English whereas 'organisation' is used in British
    English.

    Args:
        s (str): Some lowercase string.

    Returns:
        (str): `s` with -ise normalized to -ize and -isation normalized
        to -ization.
    """
    if s.endswith('ise'):
        s = s[:-3]
        s += 'ize'
    elif s.endswith('isation'):
        s = s[:-7]
        s += 'ization'
    return s


def _normalize_yse(s):
    """
    The ending -yse is British and -yze is American.

    Args:
        s (str): Some lowercase string.

    Returns:
        (str): `s` with -yse normalized to -yze.
    """
    if s.endswith('yse'):
        s = s[:-3]
        s += 'yze'
    return s


def _normalize_e(s):
    """
    Drop 'e' when comes before a suffix.

    Args:
        s: Some lowercase string.

    Returns:
        (str): `s` with all 'e' removed if `s` has more than 2
            characters.
    """
    if len(s) > 2:
        for suffix in _SUFFIXES:
            if s.endswith(suffix) and len(s) >= len(suffix) + 1 and s[:-len(suffix)][-1] == 'e':
                return s[:(-len(suffix) - 1)] + suffix
    return s


def british_normalize(s):
    """
    Args:
        s (str): Some string.

    Returns:
        (str): `s` normalized for British spelling.
    """
    s = s.lower()
    s = _normalize_our(s)
    s = _normalize_re(s)
    s = _normalize_ae_oe(s)
    s = _normalize_ise_isation(s)
    s = _normalize_yse(s)
    s = _normalize_e(s)
    return s
