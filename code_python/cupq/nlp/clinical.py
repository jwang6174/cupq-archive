"""
Identify articles that are particularly emphasized on clinical
practice.
"""

from pymongo import UpdateOne

from cupq.nlp.tokens import pre_process
from cupq.utils.dbutil import ARTICLES_COLLECTION
from cupq.utils.timelog import timelog

_CLINICAL_KEYWORDS = [
    'guideline',
    'guidelines',
    'diagnosis',
    'diagnostic',
    'diagnostics',
    'management',
    'evaluation',
    'clinical',
    'treatment',
    'treatments',
    'therapy',
    'therapies',
    'pathogenesis',
    'pathophysiology',
    'prevention',
    'preventions'
]


def has_clinical_keyword(s):
    """
    Args:
        s (str): Some string.

    Returns:
        (bool): True if `s` contains a clinical keyword.
    """
    return any([word in _CLINICAL_KEYWORDS for word in pre_process(s).split()])


def identify_titles_with_clinical_keyword():
    """
    Identify articles with at least one clinical keyword in title.
    """
    count = 0
    updates = []
    for doc in ARTICLES_COLLECTION.find({}, {'title': 1}):
        if has_clinical_keyword(doc['title']):
            updates.append(UpdateOne({'_id': doc['_id']}, {'$set': {'has_clinical_keyword': True}}))
        if len(updates) == 5000:
            ARTICLES_COLLECTION.bulk_write(updates)
            count += len(updates)
            timelog(f'INFO : updated {count} articles')
            del updates[:]
    if updates:
        ARTICLES_COLLECTION.bulk_write(updates)
        count += len(updates)
        timelog(f'INFO : updated {count} articles')


if __name__ == '__main__':
    identify_titles_with_clinical_keyword()
