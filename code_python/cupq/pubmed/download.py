"""
Download PubMed XML files and checksums. Verify file integrities and
re-download corrupt files.
"""

import os
from time import sleep
from ftputil import FTPHost
from ftputil.error import TemporaryError, FTPIOError
from hashlib import md5

from cupq.utils.pathutil import PUBMED_PATH as LOCAL_PUBMED_PATH
from cupq.utils.timelog import timelog


def _get_finished_filenames():
    """
    Returns:
        (set of str): Downloaded file names.
    """
    return set(os.listdir(LOCAL_PUBMED_PATH))


def _get_xml_checksum(path):
    """
    Args:
        path (str): XML file path.

    Returns:
        (str): Checksum of XML file.
    """
    hasher = md5()
    with open(path, 'rb') as f:
        for chunk in iter(lambda: f.read(4096), b''):
            hasher.update(chunk)
    return str(hasher.hexdigest())


def _get_md5_checksum(path):
    """
    Args:
        path (str): MD5 file path.

    Returns:
        (str): Checksum in MD5 file.
    """
    with open(path, 'r') as f:
        return f.read().split('=')[1].strip()


def _download():
    """
    Download XML and MD5 files. More recent files are downloaded first.
    """
    finished = _get_finished_filenames()
    while True:
        try:
            with FTPHost('130.14.250.10', 'anonymous', '') as host:
                for dir_ in ['/pubmed/updatefiles/', '/pubmed/baseline/']:
                    for filename in reversed(sorted(host.listdir(dir_))):
                        if (filename.endswith('.xml.gz') or filename.endswith('.xml.gz.md5')) \
                                and filename not in finished:
                            host.download(f'{dir_}/{filename}', os.path.join(LOCAL_PUBMED_PATH, filename))
                            finished.add(filename)
                            timelog(f'INFO : downloaded {filename}')
                return
        except (TemporaryError, FTPIOError):
            timelog('ERROR : problem downloading PubMed, re-trying in 60 seconds')
            sleep(60)


def _get_corrupted_paths():
    """
    Returns:
        (list of str): Paths of corrupted XML and MD5 files.
    """
    corrupted = []
    for filename in _get_finished_filenames():
        if filename.endswith('.gz'):
            xml_path = os.path.join(LOCAL_PUBMED_PATH, filename)
            md5_path = os.path.join(LOCAL_PUBMED_PATH, f'{filename}.md5')
            xml_checksum = _get_xml_checksum(xml_path)
            md5_checksum = _get_md5_checksum(md5_path)
            if xml_checksum != md5_checksum:
                corrupted.append(xml_path)
                corrupted.append(md5_path)
    return corrupted


def download_pubmed():
    """
    Download PubMed XML files and checksums. Verify file integrities
    and re-download corrupt files.
    """
    while True:
        _download()
        corrupted_paths = _get_corrupted_paths()
        if corrupted_paths:
            for path in corrupted_paths:
                os.remove(path)
        else:
            break


if __name__ == '__main__':
    download_pubmed()
