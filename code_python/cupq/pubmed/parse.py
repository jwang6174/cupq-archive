"""
Parse select contents from PubMed XML files.
"""

import gzip
from lxml import etree

from cupq.pubmed.article import Article


class XmlDoc:
    """"
    Contents of each article contained in a XML document.

    Args:
        filepath (str): Absolute path of XML file.
    """
    def __init__(self, filepath):
        self.filepath = filepath
        self.articles = self._get_articles()

    def _get_articles(self):
        """
        Returns:
            (list of Article): Articles in the XML document.
        """
        articles = []
        with gzip.open(self.filepath, 'rb') as f:
            tree = etree.parse(f)
            root = tree.getroot()
            for elem in root.findall('./PubmedArticle'):
                articles.append(Article(elem))
            root.clear()
        return articles
