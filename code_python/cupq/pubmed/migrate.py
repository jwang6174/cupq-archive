"""
Parse PubMed XML documents and store information in MongoDB.
"""

import os
from pymongo import UpdateOne, DeleteOne

from cupq.nlp.clinical import has_clinical_keyword
from cupq.pubmed.parse import XmlDoc
from cupq.pubmed.filter import has_valid_pub_types, has_valid_pub_year, has_valid_journal, in_english
from cupq.utils.dbutil import ARTICLES_COLLECTION
from cupq.utils.pathutil import PUBMED_PATH, PUBMED_MIGRATE_LOG_PATH
from cupq.utils.timelog import timelog


def _get_log():
    """
    Returns:
        (frozenset of str): Names of migrated files.
    """
    with open(PUBMED_MIGRATE_LOG_PATH, 'a+') as f:
        f.seek(0)
        return frozenset(f.read().splitlines())


def _update_log(name):
    """
    Add name of file that has been processed to log file.

    Args:
        name (str): File name.
    """
    with open(PUBMED_MIGRATE_LOG_PATH, 'a') as f:
        return f.write(name + '\n')


def _get_xml_paths():
    """
    Returns:
        (list of str): All PubMed XML file paths. More recent files are
            listed first.
    """
    paths = []
    for filename in sorted(os.listdir(PUBMED_PATH)):
        if filename.endswith('.xml.gz'):
            paths.append(os.path.join(PUBMED_PATH, filename))
    return paths


def _migrate_one(path):
    """
    Parse PubMed XML document and store information in MongoDB. Insert
    article if it does not already exist, else update article
    information. Only articles published after a certain year are
    included.

    Args:
        path (str): XML file path.
    """

    # Collection updates.
    updates = []

    # Will store each article with the name of the XML file containing
    # it. This is useful for debugging purposes.
    filename = os.path.split(path)[1]

    # Process each article in the XML file.
    articles = XmlDoc(path).articles
    for i, article in enumerate(articles, start=1):
        article_info = {
            'title': article.title,
            'abstract': article.abstract,
            'journal': article.journal,
            'iso_abbr': article.iso_abbr,
            'issn': article.issn,
            'issn_linking': article.issn_linking,
            'authors_full': article.authors_full,
            'authors_abbr': article.authors_abbr,
            'pub_year': article.pub_year,
            'pub_month': article.pub_month,
            'pub_day': article.pub_day,
            'pub_types': article.pub_types,
            'doi': article.doi,
            'pmcid': article.pmcid,
            'filename': filename,
            'has_clinical_keyword': has_clinical_keyword(article.title),
            'languages': article.languages,
            'pending': True
        }

        # Only include articles with a valid PMID and publication year.
        if (article.pmid
                and has_valid_pub_year(article)
                and has_valid_pub_types(article)
                and has_valid_journal(article)
                and in_english(article)):

            updates.append(UpdateOne({'_id': int(article.pmid)}, {'$set': article_info}, upsert=True))

        else:
            updates.append(DeleteOne({'_id': int(article.pmid)}))

        # Bulk write updates.
        if len(updates) == 10_000 or (updates and i == len(articles)):
            ARTICLES_COLLECTION.bulk_write(updates)
            del updates[:]


def migrate_pubmed():
    """
    Parse all PubMed XML documents and store their information in
    MongoDB.
    """
    log = _get_log()
    xml_paths = _get_xml_paths()
    for i, path in enumerate(xml_paths, start=1):
        name = os.path.split(path)[1]
        if name not in log:
            _migrate_one(path)
            _update_log(name)
            timelog(f'INFO : migrated {name}')


if __name__ == '__main__':
    migrate_pubmed()
