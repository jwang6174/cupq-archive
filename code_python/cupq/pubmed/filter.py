"""
Identify articles that meet certain conditions.

Attributes:
    _JCR (dict of str, Journal): Medical journal information keyed by
        linking ISSN.
    _MIN_PUB_YEAR (int): Minimum publication year for articles to be
        stored in MongoDB.
    _VALID_PUB_TYPES (list of str): Valid publication types.
    _INVALID_PUB_TYPES (list of str): Invalid publication types.
"""

from cupq.jcr.medical import get_medical_journals

_JCR = get_medical_journals()

_MIN_PUB_YEAR = 2010

_VALID_PUB_TYPES = [
    'Review',
    'Systematic Review',
    'Guideline',
    'Practice Guideline',
    'Meta-Analysis',
    'Randomized Controlled Trial'
]

_INVALID_PUB_TYPES = [
    'Published Erratum',
    'Retracted Publication',
    'Retraction of Publication'
]


def has_valid_pub_year(article):
    """
    Args:
        article (Article): A PubMed article.

    Returns:
        (bool): True if article was published after a certain year.
    """
    return article.pub_year and int(article.pub_year) >= _MIN_PUB_YEAR


def has_valid_pub_types(article):
    """
    Args:
        article (Article): A PubMed article.

    Returns:
        (bool): True if article only contains valid publication types.
    """
    valid = False
    for pub_type in article.pub_types:
        if pub_type in _INVALID_PUB_TYPES:
            return False
        if pub_type in _VALID_PUB_TYPES:
            valid = True
    return valid


def has_valid_journal(article):
    """
    Args:
        article (Article): A PubMed article.

    Returns:
        (bool): True if article was published in a medical journal.
    """
    return article.issn_linking in _JCR or article.issn in _JCR


def in_english(article):
    """
    Args:
        article (Article): A PubMed article.

    Returns:
        (bool): True if article was written only in English.
    """
    return len(article.languages) == 1 and article.languages[0] == 'eng'
