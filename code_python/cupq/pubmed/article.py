"""
Contains the `Article` class.
"""

from lxml import etree


class Article:
    """"
    Contents of a single article in a XML document.

    Args:
        root (obj): XML root element.

    Attributes:
        root (obj): XML root element.
    """
    def __init__(self, root):
        self.root = root
        self.pmid = self._get_pmid()
        self.other_ids = self._get_other_ids()
        self.doi = self.other_ids.get('doi', '')
        self.pmcid = self.other_ids.get('pmcid', '')
        self.title = self._get_title()
        self.abstract = self._get_abstract()
        self.journal = self._get_journal()
        self.iso_abbr = self._get_iso_abbr()
        self.issn = self._get_issn()
        self.issn_linking = self._get_issn_linking()
        self.authors_full = self._get_authors_full()
        self.authors_abbr = self._get_authors_abbr()
        self.pub_year = self._get_pub_year()
        self.pub_month = self._get_pub_month()
        self.pub_day = self._get_pub_day()
        self.pub_types = self._get_pub_types()
        self.languages = self._get_languages()

    def _get_all_element_text(self, e):
        """
        Args:
            e (obj): XML element.

        Returns:
            (str): All element text, including sub-elements.
        """
        text = ''
        if e.text is not None:
            text += e.text
        for child in list(e):
            text += self._get_all_element_text(child)
        if e.tail is not None:
            text += e.tail
        return text

    def _get_pmid(self):
        """
        Returns:
            (str): Article PMID.
        """
        try:
            path = './MedlineCitation/PMID'
            return self.root.find(path).text
        except AttributeError:
            return ''

    def _get_other_ids(self):
        """
        Returns:
            (dict of str, str): Other article identifiers.
        """
        try:
            ids = {}
            path = './PubmedData/ArticleIdList/ArticleId'
            for e in self.root.findall(path):
                idtype = e.get('IdType', None)
                if idtype is not None:
                    ids[idtype] = e.text
            return ids
        except AttributeError:
            return {}

    def _get_title(self):
        """
        Returns:
            (str): Title.
        """
        try:
            path = './MedlineCitation/Article/ArticleTitle'
            title = self._get_all_element_text(self.root.find(path)).strip()
            return title
        except AttributeError:
            return ''

    def _get_abstract(self):
        """
        Returns:
            (str): Abstract.
        """
        try:
            path = './MedlineCitation/Article/Abstract/AbstractText'
            elements = self.root.findall(path)
            abstract = ' '.join([self._get_all_element_text(e).strip() for e in elements]).strip()
            return abstract
        except AttributeError:
            return ''

    def _get_issn(self):
        """
        Returns:
            (str): ISSN.
        """
        try:
            path = './MedlineCitation/Article/Journal/ISSN'
            issn = self.root.find(path).text.strip()
            return issn
        except AttributeError:
            return ''

    def _get_issn_linking(self):
        """
        Returns:
            (str): ISSN linking.
        """
        try:
            path = './MedlineCitation/MedlineJournalInfo/ISSNLinking'
            issn = self.root.find(path).text.strip()
            return issn
        except AttributeError:
            return ''

    def _get_journal(self):
        """
        Returns:
            (str): Journal title.
        """
        try:
            path = './MedlineCitation/Article/Journal/Title'
            journal = self.root.find(path).text.strip()
            return journal
        except AttributeError:
            return ''

    def _get_iso_abbr(self):
        """
        Returns:
            (str): Journal ISO abbreviation.
        """
        try:
            path = './MedlineCitation/Article/Journal/ISOAbbreviation'
            abbr = self.root.find(path).text.strip()
            return abbr
        except AttributeError:
            return ''

    def _get_pub_day(self):
        """
        Returns:
            (str): Publication day.
        """
        try:
            path = './MedlineCitation/Article/Journal/JournalIssue/PubDate/Day'
            return self.root.find(path).text
        except AttributeError:
            return ''

    def _get_pub_month(self):
        """
        Returns:
            (str): Publication month.
        """
        try:
            path = './MedlineCitation/Article/Journal/JournalIssue/PubDate/Month'
            return self.root.find(path).text
        except AttributeError:
            return ''

    def _get_pub_year(self):
        """
        Returns:
            (str): Publication year.
        """
        try:
            element = self.root.find('./MedlineCitation/Article/Journal/JournalIssue/PubDate/Year')
            if element is not None:
                return element.text
            element = self.root.find('./MedlineCitation/Article/Journal/JournalIssue/PubDate/MedlineDate')
            if element is not None:
                for word in element.text.split():
                    if word.isdigit():
                        if int(word) >= 1000:
                            return word
        except AttributeError:
            return ''

    def _get_authors_full(self):
        """
        Returns:
            (list of str): Full author names.
        """
        try:
            authors = []
            path = './MedlineCitation/Article/AuthorList/Author'
            elements = self.root.findall(path)
            for e in elements:
                try:
                    lastname = e.find('./LastName').text.strip()
                    forename = e.find('./ForeName').text.strip()
                    if lastname and forename:
                        authors.append('%s %s' % (forename, lastname))
                except etree.Error:
                    continue
            return authors
        except AttributeError:
            return []

    def _get_authors_abbr(self):
        """
        Returns:
            (list of str): Abbreviated author names.
        """
        try:
            authors = []
            path = './MedlineCitation/Article/AuthorList/Author'
            elements = self.root.findall(path)
            for e in elements:
                try:
                    lastname = e.find('./LastName').text.strip()
                    initials = e.find('./Initials').text.strip()
                    if lastname and initials:
                        authors.append('%s %s' % (initials, lastname))
                except etree.Error:
                    continue
            return authors
        except AttributeError:
            return []

    def _get_pub_types(self):
        """
        Returns:
            (list of str): Publication types.
        """
        try:
            types = []
            path = './MedlineCitation/Article/PublicationTypeList/PublicationType'
            elements = self.root.findall(path)
            for e in elements:
                pub_type = e.text.strip()
                if pub_type:
                    types.append(pub_type)
            return types
        except AttributeError:
            return []

    def _get_cites(self):
        """
        Returns:
            (list of str): PMIDs of articles that this article cites.
        """
        try:
            cites = []
            path = './MedlineCitation/CommentsCorrectionsList/CommentsCorrections'
            elements = self.root.findall(path)
            for e in elements:
                try:
                    reftype = e.get('RefType', None)
                    if reftype is not None and reftype == 'Cites':
                        pmid = e.find('./PMID').text.strip()
                        if pmid:
                            cites.append(pmid)
                except AttributeError:
                    continue
            return cites
        except AttributeError:
            return []

    def _get_languages(self):
        """
        Returns:
            (list of str): Languages in which an article was published.
        """
        try:
            languages = []
            path = './MedlineCitation/Article/Language'
            elements = self.root.findall(path)
            for e in elements:
                languages.append(e.text)
            return languages
        except AttributeError:
            return []
