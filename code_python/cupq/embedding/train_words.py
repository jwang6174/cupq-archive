"""
Train word embeddings.

Attributes:
    VECTOR_SIZE (int): Embedding dimensionality.
"""

import logging
from gensim.models import Word2Vec

from cupq.nlp.tokens import get_tokens
from cupq.utils.dbutil import ARTICLES_COLLECTION
from cupq.utils.pathutil import WORD_EMBEDDINGS_PATH

# Set up logging.
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

# Embedding dimensionality.
VECTOR_SIZE = 100


class Sentences:
    """
    Utility class for loading a lot of text into Gensim Word2Vec
    training.
    """
    def __iter__(self):
        """
        Yields:
            (list of str): Title tokens.
            (list of str): Abstract tokens.
        """
        for article in ARTICLES_COLLECTION.find():
            yield get_tokens(article['title'])
            yield get_tokens(article['abstract'])


def train_from_scratch():
    """
    Train word embeddings from scratch.
    """
    model = Word2Vec(Sentences(),
                     size=VECTOR_SIZE,
                     sg=1,
                     iter=5,
                     workers=4)
    model.save(WORD_EMBEDDINGS_PATH)


if __name__ == '__main__':
    train_from_scratch()
